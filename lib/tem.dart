class Tem {
  final String addr;
  final String ph;
  final String oxy;
  final String co2;
  final String nh3;
  final String ma;
  const Tem({
    this.addr,
    this.ph,
    this.oxy,
    this.co2,
    this.nh3,
    this.ma
});
}