import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shirmp/model/loginModel.dart';

class LoginServices {
  static Future<LoginModel> postLogin(username,password) async {
    var data = {
      "username": username,
      "password": password
    };
    try{
      final response = (await http.post('http://35.222.11.224:5001/tlts/api/auth/get-token',headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },body: json.encode(data)));
      if (response.statusCode == 200) {
        print(response.body);
        return LoginModel.fromJson(json.decode(response.body));
      } else {
        return LoginModel.fromJson(json.decode(response.body));
      }
    }catch(e) {
      throw Exception(e);
    }
  }

}