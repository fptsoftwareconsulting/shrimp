import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shirmp/model/registerModel.dart';

class RegisterServices {
  static Future<RegisterModel> registerPost(username,password, fullname, email, phone) async {
    var data = {
      "fullName": fullname,
      "passWord": password,
      "nickName": fullname,
      "email": email,
      "phone": phone,
      "pagodaId": "55e09dc9-b2d2-4c05-989c-9b6f844a6bc1"
    };
    print(data);
    try{
      final response = (await http.post('http://35.222.11.224:5001/tlts/api/client-api/register',headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },body: json.encode(data)));
      if (response.statusCode == 200) {
        print(response.body);
        return RegisterModel.fromJson(json.decode(response.body));
      } else {
        return RegisterModel.fromJson(json.decode(response.body));
      }
    }catch(e) {
      throw Exception(e);
    }
  }

}