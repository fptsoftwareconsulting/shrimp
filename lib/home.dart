import 'package:flutter/material.dart';
import 'package:shirmp/model/weatherModel.dart';
import 'package:shirmp/services/weatherServices.dart';

import 'CustomShapeClipper.dart';
import 'data.dart';

final Color discountBackgroundColor = Color(0xFFFFE08D);
final Color flightBorderColor = Color(0xFFE6E6E6);
final Color chipBackgroundColor = Color(0xFFF6F6F6);
final Color firstColor = Color(0xFFF47D15);
final Color secondColor = Color(0xFFEF772C);
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nhiệt độ"),
        backgroundColor: Color(0xFFF3791A),
        elevation: 0.0,
        centerTitle: true,
        leading: InkWell(
          child: Icon(Icons.arrow_back),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            topBar(),
            contentWeather(),
            SizedBox(
              height: 20.0,
            ),
//            FlightListingBottomPart(),
          ],
        ),
      ),
    );
  }
}
class topBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [firstColor, secondColor])
            ),
            height: 100,
          ),
        )
      ],
    );
  }
}
class contentWeather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Text("Danh sách nhiệt độ theo ngày", style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),),
          SizedBox(height: 20.0,),
          ListWeather(),
          SizedBox(height: 20.0,),

        ],
      ),
    );
  }
}
class ListWeather extends StatelessWidget {
  final List<String> trip = ["TPHCM", "HN", "DN"];
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WeatherModel>(
        future: WeatherServices.getWeathers(),
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return  new ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: snapshot.data.data.length,
                itemBuilder: (BuildContext context, int index) {
                 return Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        border: Border.all(color: flightBorderColor)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                      child: Stack(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Cập nhật lúc'),
                                Text('Thời gian: ${snapshot.data.data[index].createdAt}', style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16
                                ),),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Độ ẩm ${snapshot.data.data[index].ma}'),
                                Text('EC: 0.426', style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16
                                ),),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Độ ẩm không khí'),
                                Text('96.038', style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16
                                ),),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 80, 0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    Icon(Icons.kitchen, color: Colors.green[500]),
                                    Text('Độ PH'),
                                    Text('${snapshot.data.data[index].ph}', style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16
                                    ),),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Icon(Icons.ac_unit, color: Colors.green[500]),
                                    Text('Nhiệt độ'),
                                    Text('17.045', style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16
                                    ),),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Icon(Icons.brightness_high, color: Colors.green[500]),
                                    Text('Ánh sáng'),
                                    Text('0.0', style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16
                                    ),),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          }else {
            return Container(
              child: Center(
                child: Text("Đang tải..."),
              ),
            );
          }

          return Container();
    });
  }
}


