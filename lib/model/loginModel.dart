class LoginModel {
  String accessToken;
  int expiry;
  String refreshToken;

  LoginModel({this.accessToken, this.expiry, this.refreshToken});

  LoginModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    expiry = json['expiry'];
    refreshToken = json['refreshToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accessToken'] = this.accessToken;
    data['expiry'] = this.expiry;
    data['refreshToken'] = this.refreshToken;
    return data;
  }
}