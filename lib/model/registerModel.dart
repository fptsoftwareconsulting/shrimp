class RegisterModel {
  bool success;
  List<ErrorLists> errorLists;

  RegisterModel({this.success, this.errorLists});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['errorLists'] != null) {
      errorLists = new List<ErrorLists>();
      json['errorLists'].forEach((v) {
        errorLists.add(new ErrorLists.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.errorLists != null) {
      data['errorLists'] = this.errorLists.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ErrorLists {
  String propertyName;
  String errorCode;
  String errorMessage;

  ErrorLists({this.propertyName, this.errorCode, this.errorMessage});

  ErrorLists.fromJson(Map<String, dynamic> json) {
    propertyName = json['propertyName'];
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['propertyName'] = this.propertyName;
    data['errorCode'] = this.errorCode;
    data['errorMessage'] = this.errorMessage;
    return data;
  }
}