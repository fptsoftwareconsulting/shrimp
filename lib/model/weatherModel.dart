class WeatherModel {
  int status;
  List<Data> data;

  WeatherModel({this.status, this.data});

  WeatherModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int createdAt;
  String sId;
  String addr;
  String ph;
  String oxy;
  String co2;
  String nh3;
  String ma;
  int iV;

  Data(
      {this.createdAt,
        this.sId,
        this.addr,
        this.ph,
        this.oxy,
        this.co2,
        this.nh3,
        this.ma,
        this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    sId = json['_id'];
    addr = json['addr'];
    ph = json['ph'];
    oxy = json['oxy'];
    co2 = json['co2'];
    nh3 = json['nh3'];
    ma = json['ma'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['_id'] = this.sId;
    data['addr'] = this.addr;
    data['ph'] = this.ph;
    data['oxy'] = this.oxy;
    data['co2'] = this.co2;
    data['nh3'] = this.nh3;
    data['ma'] = this.ma;
    data['__v'] = this.iV;
    return data;
  }
}